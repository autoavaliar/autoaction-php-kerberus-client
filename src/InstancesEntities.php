<?php

namespace AutoAction\Cerberus;

class InstancesEntities
{

    const uriInstanceEntities = "/api/v1/instance-entities";

    /**
     * @param $instance
     * @param $entities
     * @param $cerberusApiHost
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function upsert($instance, $entities, $cerberusApiHost, $serviceAccountToken)
    {
        foreach ($entities as &$entity) {
            $entity = [
                "name" => isset($entity["name"]) ? $entity["name"] : null,
                "full_name" => isset($entity["fullName"]) ? $entity["fullName"] : null,
                "email" => isset($entity["email"]) ? $entity["email"] : null,
                "document" => isset($entity["document"]) ? $entity["document"] : null,
                "status" => isset($entity["flagStatus"]) ? $entity["flagStatus"] : 1,
                "ie" => isset($entity["ie"]) ? $entity["ie"] : null,
                "site" => isset($entity["site"]) ? $entity["site"] : null,
                "zip" => isset($entity["zip"]) ? $entity["zip"] : null,
                "address" => isset($entity["address"]) ? $entity["address"] : null,
                "address_number" => isset($entity["addressNumber"]) ? $entity["addressNumber"] : null,
                "address_details" => isset($entity["addressDetails"]) ? $entity["addressDetails"] : null,
                "town" => isset($entity["town"]) ? $entity["town"] : null,
                "city" => isset($entity["city"]) ? $entity["city"] : null,
                "contact_name" => isset($entity["contactName"]) ? $entity["contactName"] : null,
                "contact_country_code" => isset($entity["contactCountryCode"]) ? $entity["contactCountryCode"] : null,
                "contact_ddd" => isset($entity["contactDdd"]) ? $entity["contactDdd"] : null,
                "contact_phone" => isset($entity["contactPhone"]) ? $entity["contactPhone"] : null,
                "ego_instance_id" => isset($entity["egoInstanceId"]) ? $entity["egoInstanceId"] : null,
                "ego_entity_id" => isset($entity["egoEntityId"]) ? $entity["egoEntityId"] : null,
                "admnew_instance_id" => isset($entity["admNewInstanceId"]) ? $entity["admNewInstanceId"] : null,
                "admnew_entity_id" => isset($entity["admNewEntityId"]) ? $entity["admNewEntityId"] : null,
                "country_id" => isset($entity["countryId"]) ? $entity["countryId"] : null
            ];
        }
        $data = [
            "instance" => [
                "name" => isset($instance["name"]) ? $instance["name"] : null,
                "email" => isset($instance["email"]) ? $instance["email"] : null,
                "site" => isset($instance["site"]) ? $instance["site"] : null,
                "status" => isset($instance["flagStatus"]) ? $instance["flagStatus"] : 1,
                "contact_name" => isset($instance["contactName"]) ? $instance["contactName"] : null,
                "contact_phone" => isset($instance["contactPhone"]) ? $instance["contactPhone"] : null,
                "ego_instance_id" => isset($instance["egoInstanceId"]) ? $instance["egoInstanceId"] : null,
                "admnew_instance_id" => isset($instance["admNewInstanceId"]) ? $instance["admNewInstanceId"] : null
            ],
            "entities" => $entities
        ];
        if (isset($instance["countryId"])){
            $data["instance"]["country_id"] = $instance["countryId"];
        }
        $headers = ['Authorization' => 'Bearer ' . $serviceAccountToken, 'Content-Type' => 'application/json'];
        $response = Request::request('POST', $cerberusApiHost . self::uriInstanceEntities, $data, $headers);
        if (isset($response["entities"]) && isset($response["entities"]["errors"]) && count($response["entities"]["errors"])) {
            throw new \Exception('Error upserting entity ' . implode(",", $response["entities"]["errors"]));
        }
        return $response;
    }

}