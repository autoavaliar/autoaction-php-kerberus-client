<?php

namespace AutoAction\Cerberus;

class Request
{

    /**
     * @param $method
     * @param $url
     * @param $body
     * @param $headers
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function request($method, $url, $body = [], $headers = [])
    {
        try {
            $client = new \GuzzleHttp\Client(['verify' => false]);
            $response = $client->request($method, $url, ['headers' => $headers, 'body' => json_encode($body)]);
            if ($response->getStatusCode() != 200) {
                throw new \Exception($response);
            }
            $contents = $response->getBody()->getContents();
            $result = json_decode($contents, true)["data"];
            if ($result && isset($result['errors'])) {
                throw new \Exception(implode(", ", $result['errors']));
            }
            return $result;
        } catch (\Exception $error) {
            $errorMsg = $error->getMessage() . ' - ' . $error->getTraceAsString();
            throw new \Exception('Error in request to ' . $url . ' - ' . $errorMsg);
        }
    }

}