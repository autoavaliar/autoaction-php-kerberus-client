<?php

namespace AutoAction\Cerberus;

class Client
{

    const cacheKeyBannedUsers = "cerberus_banned_jwt_";

    /**
     * Essa função foi implementada com base no client já existente de golang
     * Porém não está sendo utilizada no momento, tendo sido substituída pela validateTokens
     *
     * @param $appEnvironment
     * @param $accessToken
     * @param $refreshToken
     * @param $publicRSAKey
     * @param $redisHost
     * @param $redisPort
     * @param $redisPassword
     * @param $cerberusApiHost
     * @return array
     */
    public static function checkTokens(
        $appEnvironment,
        $accessToken,
        $refreshToken,
        $publicRSAKey,
        $redisHost,
        $redisPort,
        $redisPassword,
        $cerberusApiHost
    ) {
        $parsedAccessToken = Utils::parseJwt($accessToken, $publicRSAKey);
        $redisClient = Redis::getRedisClient($redisHost, $redisPort, $redisPassword);
        self::checkBannedToken($appEnvironment, $redisClient, $parsedAccessToken['jwt_token_id']);

        Utils::parseJwt($refreshToken, $publicRSAKey);
        $response = Auth::refreshToken($accessToken, $refreshToken, $cerberusApiHost);
        return [
            'parsed' => Utils::parseJwt($response["access_token"], $publicRSAKey),
            'accessToken' => $response["access_token"],
            'refreshToken' => $response["refresh_token"],
        ];
    }

    /**
     * @param $appEnvironment
     * @param $accessToken
     * @param $refreshToken
     * @param $publicRSAKey
     * @param $redisHost
     * @param $redisPort
     * @param $redisPassword
     * @param $cerberusApiHost
     * @return array
     */
    public static function validateTokens(
        $appEnvironment,
        $accessToken,
        $refreshToken,
        $publicRSAKey,
        $redisHost,
        $redisPort,
        $redisPassword,
        $cerberusApiHost
    ) {
        $parsed = null;
        try {
            $parsed = Utils::parseJwt($accessToken, $publicRSAKey);
        } catch (\Exception $err) {
            Utils::parseJwt($refreshToken, $publicRSAKey);
            $response = Auth::refreshToken($accessToken, $refreshToken, $cerberusApiHost);
            $accessToken = $response["access_token"];
            $refreshToken = $response["refresh_token"];
            $parsed = Utils::parseJwt($accessToken, $publicRSAKey);
        }
        $redisClient = Redis::getRedisClient($redisHost, $redisPort, $redisPassword);
        self::checkBannedToken($appEnvironment, $redisClient, $parsed['jwt_token_id']);
        return [
            'parsed' => $parsed,
            'accessToken' => $accessToken,
            'refreshToken' => $refreshToken
        ];
    }

    /**
     * @param $appEnvironment
     * @param $redisClient
     * @param $jwtTokenId
     * @return void
     */
    public static function checkBannedToken($appEnvironment, $redisClient, $jwtTokenId)
    {
        $cacheKey = $appEnvironment . self::cacheKeyBannedUsers . $jwtTokenId;
        if ($redisClient->get($cacheKey)) {
            throw new \Exception('Banned token');
        }
    }

    /**
     * @param $appEnvironment
     * @param $jwtTokenId
     * @param $redisHost
     * @param $redisPort
     * @param $redisPassword
     * @return void
     */
    public static function setBannedToken($appEnvironment, $jwtTokenId, $redisHost, $redisPort, $redisPassword)
    {
        $redisClient = Redis::getRedisClient($redisHost, $redisPort, $redisPassword);
        $banned = ["banned_token_id" => $jwtTokenId];
        $cacheKey = $appEnvironment . self::cacheKeyBannedUsers . $jwtTokenId;
        Redis::redisSet($redisClient, $cacheKey, $banned, Redis::HOUR * 1);
    }
}