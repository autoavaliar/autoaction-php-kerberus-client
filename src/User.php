<?php

namespace AutoAction\Cerberus;

class User
{

    const uriUsersExternal = "/api/v1/users/external";
    const uriUserInformation = "/api/v1/userinfo";
    const cacheKeyInfoUser = "cerberus_jwt_user_id_";

    /**
     * @param $user
     * @param $cerberusApiHost
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function upsert($serviceAccountAccessToken, $user = [], $cerberusApiHost)
    {
        $data = [
            "login" => isset($user["login"]) ? $user["login"] : null,
            "email" => isset($user["email"]) ? $user["email"] : null,
            "password" => isset($user["password"]) ? $user["password"] : null,
            "name" => isset($user["name"]) ? $user["name"] : null,
            "document" => isset($user["document"]) ? $user["document"] : null,
            "entity_id" => isset($user["entityId"]) ? $user["entityId"] : null,
            "ego_entity_id" => isset($user["egoEntityId"]) ? $user["egoEntityId"] : null,
            "admnew_entity_id" => isset($user["admNewEntityId"]) ? $user["admNewEntityId"] : null,
            "user_group_id" => isset($user["userGroupId"]) ? $user["userGroupId"] : null,
            "admnew_user_id" => isset($user["admNewUserId"]) ? $user["admNewUserId"] : null,
            "ego_user_id" => isset($user["egoUserId"]) ? $user["egoUserId"] : null,
            "application_id" => isset($user["applicationId"]) ? $user["applicationId"] : null,
            "application_initials" => isset($user["applicationInitials"]) ? $user["applicationInitials"] : null,
            "country_id" => isset($user["country_id"]) ? $user["country_id"] : null,
            "flags" => isset($user["flags"]) ? $user["flags"] : []
        ];
        $headers = ['Authorization' => 'Bearer ' . $serviceAccountAccessToken, 'Content-Type' => 'application/json'];
        return Request::request('POST', $cerberusApiHost . self::uriUsersExternal, $data, $headers);
    }

    /**
     * @param $accessToken
     * @param $application
     * @param $cerberusApiHost
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private static function getUserInfo($accessToken, $application, $cerberusApiHost)
    {
        $body = ["application" => $application];
        $headers = ["Authorization" => "Bearer " . $accessToken];
        return Request::request('GET', $cerberusApiHost . self::uriUserInformation, $body, $headers);
    }

    /**
     * @param $appEnvironment
     * @param $accessToken
     * @param $refreshToken
     * @param $publicRSAKey
     * @param $redisHost
     * @param $redisPort
     * @param $redisPassword
     * @param $cerberusApiHost
     * @return array
     */
    public static function getUserInformation(
        $appEnvironment,
        $accessToken,
        $refreshToken,
        $publicRSAKey,
        $redisHost,
        $redisPort,
        $redisPassword,
        $cerberusApiHost
    ) {
        /*$result = Client::checkTokens($appEnvironment, $accessToken, $refreshToken, $publicRSAKey, $redisHost, $redisPort, $redisPassword, $cerberusApiHost);*/
        $result = Client::validateTokens($appEnvironment, $accessToken, $refreshToken, $publicRSAKey, $redisHost,
            $redisPort, $redisPassword, $cerberusApiHost);

        $redisClient = Redis::getRedisClient($redisHost, $redisPort, $redisPassword);
        $cacheKey = $appEnvironment . self::cacheKeyInfoUser . $result['parsed']['user_id'];
        $user = $redisClient->get($cacheKey);
        if (!$user) {
            $user = self::getUserInfo($result['accessToken'], $result['parsed']['application_id'], $cerberusApiHost);
            Redis::redisSet($redisClient, $cacheKey, $user, Redis::HOUR * 2);
        }

        return [
            'user' => is_string($user) ? json_decode($user, true) : $user,
            'accessToken' => $result['accessToken'],
            'refreshToken' => $result['refreshToken'],
        ];
    }

    /**decode
     * @param $appEnvironment
     * @param $userId
     * @param $redisHost
     * @param $redisPort
     * @param $redisPassword
     * @return void
     */
    public static function deleteUserInformation($appEnvironment, $userId, $redisHost, $redisPort, $redisPassword)
    {
        $redisClient = Redis::getRedisClient($redisHost, $redisPort, $redisPassword);
        $cacheKey = $appEnvironment . self::cacheKeyInfoUser . $userId;
        $redisClient->del($cacheKey);
    }

}