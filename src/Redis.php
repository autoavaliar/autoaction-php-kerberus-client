<?php

namespace AutoAction\Cerberus;

class Redis
{
    const HOUR = 60 * 60;

    /**
     * @param $host
     * @param $port
     * @param $password
     * @return \Predis\Client
     */
    public static function getRedisClient($host, $port, $password)
    {
        return new \Predis\Client([
            'scheme' => 'tcp',
            'host' => $host,
            'port' => $port,
            'password' => $password
        ]);
    }

    /**
     * @param $redisClient
     * @param $key
     * @param $val
     * @param $timeout
     * @return mixed
     */
    public static function redisSet($redisClient, $key, $val, $timeout = 60 * 60)
    {
        if (is_object($val) || is_array($val)) {
            $val = json_encode($val);
        }
        return $redisClient->setex($key, $timeout, $val);
    }

}