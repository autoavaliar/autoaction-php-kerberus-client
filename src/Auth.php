<?php

namespace AutoAction\Cerberus;

class Auth
{
    const uriCSRF = "/api/v1/csrf";
    const uriAuth = "/api/v2/auth";
    const uriRefreshToken = "/api/v1/refresh-token";
    const uriLogout = '/api/v1/logout';
    const KACCESS_TOKEN = 'kaccess_token';
    const KREFRESH_TOKEN = 'krefresh_token';

    /**
     * @param $cerberusApiHost
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function CSRF($cerberusApiHost)
    {
        $response = Request::request('GET', $cerberusApiHost . self::uriCSRF);
        return $response['csrf_token'];
    }

    /**
     * @param $login
     * @param $password
     * @param $application
     * @param $cerberusApiHost
     * @return mixed
     */
    public static function auth($login, $password, $application, $cerberusApiHost, $countryId = null)
    {
        $body = ["login" => $login, "password" => $password, "application" => $application, "country_id" => $countryId];
        $headers = ["X-CSRF-TOKEN" => self::CSRF($cerberusApiHost), 'Content-Type' => 'application/json'];
        return Request::request('POST', $cerberusApiHost . self::uriAuth, $body, $headers);
    }

    /**
     * @param $accessToken
     * @param $refreshToken
     * @param $cerberusApiHost
     * @return mixed
     */
    public static function refreshToken(
        $accessToken,
        $refreshToken,
        $cerberusApiHost
    ) {
        return Request::request('POST', $cerberusApiHost . self::uriRefreshToken,
            self::getAuthBody($accessToken, $refreshToken));
    }

    /**
     * @param $accessToken
     * @param $refreshToken
     * @param $cerberusApiHost
     * @return mixed
     */
    public static function logout(
        $accessToken,
        $refreshToken,
        $cerberusApiHost
    ) {
        $headers = ['Authorization' => 'Bearer ' . $accessToken, 'Content-Type' => 'application/json'];
        return Request::request('POST', $cerberusApiHost . self::uriLogout,
            self::getAuthBody($accessToken, $refreshToken),
            $headers);
    }

    /**
     * @param $accessToken
     * @param $refreshToken
     * @return array
     */
    public static function getAuthBody($accessToken, $refreshToken)
    {
        return ["access_token" => $accessToken, "refresh_token" => $refreshToken];
    }


    /**
     * @param $accessToken
     * @param $refreshToken
     * @param $domain
     * @param $secure
     * @return array
     */
    public static function saveTokens($accessToken, $refreshToken, $domain = "", $secure = true)
    {
        setcookie(self::KACCESS_TOKEN, $accessToken, time() + 1 * 60 * 60 + 1800, "/", $domain, $secure, true);
        setcookie(self::KREFRESH_TOKEN, $refreshToken, time() + 3 * 60 * 60, "/", $domain, $secure, true);
        return [
            'accessToken' => $accessToken,
            'refreshToken' => $refreshToken
        ];
    }

    /**
     * @param $domain
     * @param $secure
     * @return void
     */
    public static function clearTokens($domain = "", $secure = true)
    {
        setcookie(self::KACCESS_TOKEN, '', time() - 60 * 60, "/", $domain, $secure, true);
        setcookie(self::KREFRESH_TOKEN, '', time() - 60 * 60, "/", $domain, $secure, true);
    }
}
