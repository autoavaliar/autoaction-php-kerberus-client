<?php

namespace AutoAction\Cerberus;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class Utils
{
    /**
     * @param $token
     * @param $publicRSAKey
     * @return mixed
     */
    public static function parseJwt($token, $publicRSAKey)
    {
        $decoded = JWT::decode($token, new Key(base64_decode($publicRSAKey), 'RS256'));
        if (($decoded->exp ? $decoded->exp : 0) < (new \DateTime())->getTimestamp()) {
            throw new \Exception('Expired token, please login again');
        }
        return (array) $decoded;
    }

}