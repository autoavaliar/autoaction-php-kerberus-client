<?php

error_reporting(E_ALL);
ini_set('display_errors', 'on');

require 'vendor/autoload.php';

$host = "";
$key = "";
$environment = "develop";

$login = "";
$password = "";
$application = "";

$redisHost = "127.0.0.1";
$redisPort = "6379";
$redisPassword = "";

$result = \AutoAction\Cerberus\Auth::auth($login, $password, $application, $host);
$accessToken = $result["access_token"];
$refreshToken = $result["refresh_token"];

$result2 = \AutoAction\Cerberus\Auth::refreshToken($accessToken, $refreshToken, $host);

/*$result3 = AutoAction\Cerberus\Auth::logout($accessToken, $refreshToken, $host);
var_dump($result3);*/

$result4 = \AutoAction\Cerberus\Client::checkTokens($environment, $accessToken, $refreshToken, $key, $redisHost,
    $redisPort,
    $redisPassword, $host);

$result5 = \AutoAction\Cerberus\Client::validateTokens($environment, $accessToken, $refreshToken, $key, $redisHost,
    $redisPort,
    $redisPassword, $host);

$result6 = \AutoAction\Cerberus\User::getUserInformation($environment, $accessToken, $refreshToken, $key, $redisHost,
    $redisPort,
    $redisPassword, $host);

$redisClient = \AutoAction\Cerberus\Redis::getRedisClient($redisHost, $redisPort, $redisPassword);

//$result7 = \AutoAction\Cerberus\Client::setBannedToken($environment, '123', $redisHost, $redisPort, $redisPassword);

$result8 = \AutoAction\Cerberus\Client::checkBannedToken($environment, $redisClient, '123456');

$result9 = \AutoAction\Cerberus\User::deleteUserInformation($environment, '123', $redisHost, $redisPort,
    $redisPassword);