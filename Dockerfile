FROM php:5.6-cli-alpine

# Install dependencies required for Composer
RUN apk update && apk add curl $PHPIZE_DEPS

# Install the PHP Redis extension
RUN pecl install redis-2.2.8 && docker-php-ext-enable redis

# Download and install Composer installer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/html

COPY . /var/www/html

RUN composer install